import {Sudoku} from "./sudoku";

const sudoku = new Sudoku([
    [8, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 3, 6, 0, 0, 0, 0, 0],
    [0, 7, 0, 0, 9, 0, 2, 0, 0],
    [0, 5, 0, 0, 0, 7, 0, 0, 0],
    [0, 0, 0, 0, 4, 5, 7, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 3, 0],
    [0, 0, 1, 0, 0, 0, 0, 6, 8],
    [0, 0, 8, 5, 0, 0, 0, 1, 0],
    [0, 9, 0, 0, 0, 0, 4, 0, 0],
]);

let wasmResultText, jsResultText;

window.onload = () => {
    initSudokuGrid();
    initSolveButton();
    initClearButton();
    updateView();
};

function initSudokuGrid() {
    const grid = document.querySelector('#sudoku');

    for (let y = 0; y < 9; y++) {
        for (let x = 0; x < 9; x++) {
            const cell = grid.appendChild(document.createElement('div'));
            cell.setAttribute('data-x', String(x));
            cell.setAttribute('data-y', String(y));

            const applyBlackGridLines = (n, frontBorder, backBorder) => {
                const border = size => `${size}px solid black`;

                if (n === 0) cell.style[frontBorder] = border(2);
                else if (n === 8) cell.style[backBorder] = border(2);
                else {
                    if (n % 3 === 0) cell.style[frontBorder] = border(1);
                    else if (n % 3 === 2) cell.style[backBorder] = border(1);
                }
            };

            applyBlackGridLines(y, 'borderTop', 'borderBottom');
            applyBlackGridLines(x, 'borderLeft', 'borderRight');

            cell.onclick = (_) => {
                if (sudoku.selectedField === cell) sudoku.selectedField = null;
                else sudoku.selectedField = cell;

                sudoku.solution = null;
                updateView();
            };
        }
    }
}

function initSolveButton() {
    document.querySelector('button#solve').onclick = () => {
        sudoku.selectedField = null;
        document.querySelector('#result').style.visibility = 'visible';

        solveAndDisplay(false).then(() => {
            solveAndDisplay(true);
        });
    };
}

function solveAndDisplay(useJavaScriptAlgorithm) {
    return new Promise(resolve => {
        let resultText;

        sudoku.solve(useJavaScriptAlgorithm).then(result => {
            sudoku.solution = result.sudoku.array;
            resultText = `Solved in ${result.passedSeconds} seconds`;
        }).catch(error => {
            if (error.userInfo) {
                resultText = error.userInfo;
            } else {
                resultText = 'Error';
                console.error(error);
            }
        }).finally(() => {
            if (useJavaScriptAlgorithm) jsResultText = resultText;
            else wasmResultText = resultText;

            updateView();
            resolve();
        });
    });
}

function initClearButton() {
    document.querySelector('button#clear').onclick = () => {
        sudoku.selectedField = null;
        sudoku.solution = null;
        sudoku.clear();
        updateView();
    };
}

function updateView() {
    showSudoku(sudoku);
    document.querySelector('#result > #wasm').textContent = wasmResultText;
    document.querySelector('#result > #js').textContent = jsResultText;
}

function showSudoku(sudoku) {
    const cells = document.querySelectorAll('#sudoku > div');
    const sudokuArray = sudoku.solution ? sudoku.solution : sudoku.array;

    cells.forEach((cell, n) => {
        const y = Math.floor(n / 9);
        const x = n % 9;

        const number = sudokuArray[y][x];
        if (number > 0) cell.textContent = number;
        else cell.textContent = '';

        if (sudoku.selectedField === cell) cell.classList.add('selected');
        else cell.classList.remove('selected');
    });
}

document.onkeydown = (event) => {
    const number = Number(event.key);

    if (sudoku.selectedField) {
        const x = sudoku.selectedField.getAttribute('data-x');
        const y = sudoku.selectedField.getAttribute('data-y');

        if (number) {
            sudoku.array[y][x] = number;
        } else if (number === 0 || event.key === 'Delete') {
            sudoku.array[y][x] = 0;
        } else if (event.key === 'Escape' || event.key === 'Enter') {
            sudoku.selectedField = null;
        }

        updateView();
    }
};
