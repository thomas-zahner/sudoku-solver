const wasm = import('./../pkg/sudoku_solver_wasm.js');

const SolutionState = {None: "None", One: "One", Multiple: "Multiple"};

export function solveWithWebAssembly(sudoku) {
    return new Promise((resolve, reject) => {
        wasm.then(wasm => {
            try {
                wasm.init();
                const result = wasm.solve(sudoku.array);
                handlePromise(result, resolve, reject);
            } catch (e) {
                reject(e);
            }
        });
    });
}

export function solveWithJavaScript(sudoku) {
    return new Promise((resolve, reject) => {
        const result = {state: SolutionState.None, solution: null};

        if (isValid(sudoku)) {
            calculateSolution(sudoku.clone(), result);
        }

        handlePromise(result, resolve, reject);
    });
}

function handlePromise(result, resolve, reject) {
    if (result.state === SolutionState.One) resolve(result.solution);
    else if (result.state === SolutionState.None) reject({userInfo: 'No solution found'});
    else if (result.state === SolutionState.Multiple) reject({userInfo: 'There is more than one solution'});
}

function isValid(sudoku) {
    for (let x = 0; x < 9; x++) {
        for (let y = 0; y < 9; y++) {
            const number = sudoku.array[y][x];
            if (number > 0) {
                if (!isPossible(sudoku, {x, y}, number)) return false;
            }
        }
    }
    return true;
}

function calculateSolution(sudoku, result) {
    for (let x = 0; x < 9; x++) {
        for (let y = 0; y < 9; y++) {
            if (sudoku.array[y][x] === 0) {
                for (let number = 1; number < 10; number++) {
                    if (isPossible(sudoku, {x, y}, number) && result.state !== SolutionState.Multiple) {
                        sudoku.array[y][x] = number;
                        calculateSolution(sudoku, result);

                        //The move didn't work -> undo it
                        sudoku.array[y][x] = 0;
                    }
                }

                //Dead end
                return;
            }
        }
    }

    result.solution = sudoku.clone().array;

    if (result.state === SolutionState.None) result.state = SolutionState.One;
    else if (result.state === SolutionState.One) result.state = SolutionState.Multiple;
}

function isPossible(sudoku, position, number) {
    for (let x = 0; x < 9; x++) {
        if (x === position.x) continue;
        if (sudoku.array[position.y][x] === number) return false;
    }

    for (let y = 0; y < 9; y++) {
        if (y === position.y) continue;
        if (sudoku.array[y][position.x] === number) return false;
    }

    const subSquare = {
        x: Math.trunc(position.x / 3),
        y: Math.trunc(position.y / 3),
    };

    const positionOffset = {
        x: position.x - subSquare.x * 3,
        y: position.y - subSquare.y * 3,
    };

    for (let x = 0; x < 3; x++) {
        for (let y = 0; y < 3; y++) {
            if (x === positionOffset.x && y === positionOffset.y) continue;
            if (sudoku.array[subSquare.y * 3 + y][subSquare.x * 3 + x] === number) {
                return false;
            }
        }
    }

    return true;
}