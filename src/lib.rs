extern crate wasm_bindgen;
extern crate console_error_panic_hook;
#[macro_use]
extern crate serde_derive;

use wasm_bindgen::prelude::*;

type Sudoku = [[u8; 9]; 9];

#[derive(Serialize, PartialEq)]
pub enum SolutionState {
    None,
    One,
    Multiple,
}

#[derive(Serialize)]
pub struct Result {
    solution: Option<Sudoku>,
    state: SolutionState,
}

struct Position {
    x: usize,
    y: usize,
}

#[wasm_bindgen]
pub fn init() {
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
}

#[wasm_bindgen]
pub fn solve(sudoku: JsValue) -> JsValue {
    let mut sudoku: Sudoku = JsValue::into_serde(&sudoku).unwrap();

    let mut result = (None, SolutionState::None);
    if is_valid(&sudoku) { result = get_solution(&mut sudoku); }

    JsValue::from_serde(&Result { solution: result.0, state: result.1 }).unwrap()
}

fn is_valid(sudoku: &Sudoku) -> bool {
    for x in 0..9 {
        for y in 0..9 {
            if sudoku[y][x] > 0 {
                if !is_possible(&sudoku, &Position { x, y }, sudoku[y][x]) {
                    return false;
                }
            }
        }
    }

    true
}

fn get_solution(sudoku: &mut Sudoku) -> (Option<Sudoku>, SolutionState) {
    let mut state = SolutionState::None;
    let mut solution = sudoku.clone();

    calculate_solution(sudoku, &mut state, &mut solution);

    return match state {
        SolutionState::One => (Some(solution), state),
        _ => (None, state)
    };
}

fn calculate_solution(sudoku: &mut Sudoku, state: &mut SolutionState, solution: &mut Sudoku) {
    for x in 0..9 {
        for y in 0..9 {
            if sudoku[y][x] == 0 {
                for number in 1..10 {
                    if is_possible(&sudoku, &Position { x, y }, number) && state != &SolutionState::Multiple {
                        sudoku[y][x] = number;
                        calculate_solution(sudoku, state, solution);

                        //The move didn't work -> undo it
                        sudoku[y][x] = 0;
                    }
                }

                //Dead end
                return;
            }
        }
    }

    *solution = sudoku.clone();

    match state {
        SolutionState::None => *state = SolutionState::One,
        SolutionState::One => *state = SolutionState::Multiple,
        SolutionState::Multiple => {}
    }
}

fn is_possible(sudoku: &Sudoku, position: &Position, number: u8) -> bool {
    for x in 0..9 {
        if x == position.x { continue; }
        if sudoku[position.y][x] == number { return false; }
    }

    for y in 0..9 {
        if y == position.y { continue; }
        if sudoku[y][position.x] == number { return false; }
    }

    let sub_square = Position {
        x: position.x / 3,
        y: position.y / 3,
    };

    let position_offset = Position {
        x: position.x - sub_square.x * 3,
        y: position.y - sub_square.y * 3,
    };

    for x in 0..3 {
        for y in 0..3 {
            if x == position_offset.x && y == position_offset.y { continue; }
            if sudoku[sub_square.y * 3 + y][sub_square.x * 3 + x] == number { return false; }
        }
    }

    true
}
