# Sudoku solver

You ever really needed to solve a Sudoku but didn't want to? Me neither. Nonetheless this project tries to address this problem. Some HTML, CSS and JavaScript was used to make a functional user interface. The algorithm however was implemented in Rust and compiled into WebAssembly using Wasm Bindgen. Later I also implemented the algorithm as JavaScript function to compare the performance between WebAssembly and JavaScript.

<img src="docs/Unsolved.png" style="zoom:67%;" />

In the following picture the solution to the puzzle of the previous screenshot has been calculated. The time required by each algorithms is shown. Note that this specific puzzle is said to be extremely difficult. Normal Sudoku puzzles are calculated in a fraction of a second. If easier puzzles are used and the algorithms require only a few milliseconds the performance difference might not always be obvious or consistent.

<img src="docs/Solved.png" style="zoom:67%;" />
